#ifndef _OMNI_CCONTENTMANAGER_HPP_
#define _OMNI_CCONTENTMANAGER_HPP_

class CContentManager 
{
public:
	static CContentManager* Get() 
	{
		if (ms_pkInstance == nullptr)
			ms_pkInstance = new CContentManager();

		return ms_pkInstance;
	}

	void Load(const char* a_szFile);

private:
	CContentManager(void);
	~CContentManager(void);

	static CContentManager* ms_pkInstance;
};

#endif