#ifndef _OMNI_CAPP_HPP_
#define _OMNI_CAPP_HPP_

class CApp
{
public:
	CApp();
	~CApp();

	bool Update();
	void Render();

private:
	CApp(const CApp& ac_rkCopy) {}

	class CWindowNT* m_pkWindow;
};

#endif