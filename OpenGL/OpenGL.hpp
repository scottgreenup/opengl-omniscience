#if defined(_WIN32) || defined(_WIN64)
	#pragma comment(lib, "opengl32.lib")
	#pragma comment(lib, "glu32.lib")
	#pragma comment(lib, "kernel32.lib")
	#pragma comment(lib, "user32.lib")
	#pragma comment(lib, "FreeImage.lib")
	
	#include <Windows.h>
	#include <gl\GL.h>
	#include <gl\GLU.h>

#elif defined(__linux) || defined(__linux__)
	#include <gl\GL.h>
	#include <gl\GLU.h>

#elif defined(__APPLE__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#endif