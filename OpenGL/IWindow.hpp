#ifndef _OMNI_IWINDOW_HPP_
#define _OMNI_IWINDOW_HPP_

class IWindow
{
public:
	IWindow();
	~IWindow();

	virtual void Resize(unsigned int a_uiWidth, unsigned int a_uiHeight) = 0;
	
	virtual void Hide() = 0;
	virtual void Show() = 0;

	virtual void SetTitle(char* a_szTitle) = 0;

protected:
	bool m_bFullscreen;

private:
	IWindow(const IWindow& ac_rkCopy) {};

};

#endif