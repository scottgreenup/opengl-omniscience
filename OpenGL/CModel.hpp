#ifndef _OMNI_CMODEL_HPP_
#define _OMNI_CMODEL_HPP_

#include "IObject.hpp"

class CModel : public IObject
{
public:
	CModel(void);
	~CModel(void);

	void Render(void);

private:

	float** m_akVerts;

};

#endif