#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "OpenGL.hpp"
#include "TextureManager.hpp"

#include "CApp.hpp"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrev, LPSTR lpCmdLine, int nCmdShow)
{
	CApp* pkApp = new CApp();

	while (pkApp->Update())
	{
		pkApp->Render();
	}

	delete pkApp;

	return 0;
}