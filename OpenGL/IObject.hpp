#ifndef _OMNI_IOBJECT_HPP_
#define _OMNI_IOBJECT_HPP_

class IObject
{
public:
	IObject() {}
	virtual ~IObject() {}

	virtual void Render() = 0;
private:

};

#endif