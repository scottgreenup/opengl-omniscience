#include "CApp.hpp"

#include "OpenGL.hpp"
#include "CWindowNT.hpp"

#include "TextureManager.hpp"

CApp::CApp()
{
	m_pkWindow = new CWindowNT("Omniscience", 1280, 720, 32, false);

	int g_iTextureID = 3;
	TextureManager::Inst()->LoadTexture(
		"C:\\Users\\Scott\\Documents\\Visual Studio 2010\\Projects\\Omniscience\\bin\\img\\uv_map.jpg", 
		g_iTextureID);

	glEnable(GL_LIGHTING);

	GLfloat LightAmbient[]= { 1.0f, 1.0f, 1.0f, 1.0f };  
	GLfloat LightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat LightDiffuse[]= { 1.0f, 0.0f, 1.0f, 1.0f };   
	GLfloat LightPosition[]= { 0.0f, 0.0f, -2.0f, 1.0f }; 

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);  
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION,LightPosition); 
	glLightfv(GL_LIGHT0, GL_SPECULAR,LightSpecular); 
	glEnable(GL_LIGHT0);

	GLfloat fogColor[4]= {0.5f, 0.5f, 0.5f, 1.0f};  
	glFogi(GL_FOG_MODE, GL_LINEAR);
	glFogfv(GL_FOG_COLOR, fogColor);
	glFogf(GL_FOG_DENSITY, 0.35f);
	glHint(GL_FOG_HINT, GL_DONT_CARE);
	glFogf(GL_FOG_START, 1.0f);
	glFogf(GL_FOG_END, 8.0f);
	glEnable(GL_FOG);
}

CApp::~CApp()
{
	delete m_pkWindow;
}

bool CApp::Update()
{
	return m_pkWindow->Tick();
}

float fRot;
float fMove = 2.0f;

void CApp::Render()
{
	m_pkWindow->Clear();

	glLoadIdentity();
	glTranslatef(0,0,-fMove);

	fMove += 0.01f;
		
	/*glRotatef(0, 
		1,
		0, 
		0);

	fRot += 0.5f;
	*/

	TextureManager::Inst()->BindTexture(3);

	glBegin(GL_QUADS);
		glNormal3f( 0.0f, 0.0f, 1.0f); 
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
	glEnd();
}

