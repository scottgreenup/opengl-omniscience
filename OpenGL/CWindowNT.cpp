#include "CWindowNT.hpp"

#include "OpenGL.hpp"

#include <Windows.h>



/*bool keys[256];
bool active = true;
bool fullscreen = true;*/

CWindowNT::CWindowNT(char* a_szTitle, int a_iWidth, int a_iHeight, int a_iBits, bool a_bFullscreen)
	: m_szTitle(a_szTitle)
	, m_iWidth(a_iWidth)
	, m_iHeight(a_iHeight)
	, m_iPixelBits(a_iBits)
	, m_bFullscreen(a_bFullscreen) // last argument
	, m_szClassName("Omniscience")
	, m_bHasFocus(false)
	, m_hInstance(NULL)
	, m_hWindow(NULL)
	, m_sWindowClass(0)
	, m_hRenderingContext(NULL)
	, m_hDeviceContext(NULL)
{
	InitWindow();
	InitContext();
	InitOpenGL();
}

CWindowNT::~CWindowNT()
{
	if (m_hRenderingContext)
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(m_hRenderingContext);
		
		m_hRenderingContext = NULL;
	}

	if (m_hDeviceContext)
	{
		ReleaseDC(m_hWindow, m_hDeviceContext);
		m_hDeviceContext = NULL;
	}

	if (m_bFullscreen)
	{
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(true);
	}

	if (m_hWindow)
	{
		DestroyWindow(m_hWindow);
		m_hWindow = NULL;
	}

	if (m_hInstance)
	{
		UnregisterClass(m_szClassName, m_hInstance);
		m_hInstance = NULL;
	}

	m_sWindowClass = NULL;
}

void CWindowNT::InitWindow()
{
	m_hInstance = GetModuleHandle(0);

	RECT kRect = {
		static_cast<long>(0),
		static_cast<long>(0),
		static_cast<long>(m_iWidth),
		static_cast<long>(m_iHeight) };

	DWORD dwExStyle;
	DWORD dwStyle;

	if (m_bFullscreen)
	{
		dwStyle = WS_POPUP;
		dwExStyle = WS_EX_APPWINDOW;
	}
	else
	{
		dwStyle = WS_SYSMENU;
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	}
	
	AdjustWindowRectEx(&kRect, dwStyle, false, dwExStyle);

	int iWidth = (kRect.right - kRect.left);
	int iHeight = (kRect.bottom - kRect.top);

	WNDCLASSEX wcex;
	memset(&wcex, 0, sizeof(wcex));
	
	wcex.cbSize			= sizeof(wcex);
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.style			= CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wcex.lpfnWndProc	= CWindowNT::WindowProc;
	wcex.hInstance		= m_hInstance;
	wcex.hIcon			= LoadIcon(0, IDI_WINLOGO);
	wcex.hCursor		= LoadCursor(0, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)GetStockObject(LTGRAY_BRUSH);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= m_szClassName;
	
	m_sWindowClass = RegisterClassEx (&wcex);
	
	if (m_sWindowClass == 0)
	{
		MessageBox(NULL, "Failed to register the Window Class.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	// center window
	RECT kScreenRect;
	GetWindowRect(GetDesktopWindow(), &kScreenRect);

	int iX, iY;

	if (m_bFullscreen)
	{
		iX = 0;
		iY = 0;
	}
	else
	{
		iX = ( (kScreenRect.right - kScreenRect.left) - iWidth ) >> 1;
		iY = ( (kScreenRect.bottom - kScreenRect.top) - iHeight ) >> 1;
	}

	m_hWindow = CreateWindowEx(
		dwExStyle,
		MAKEINTATOM(m_sWindowClass),
		m_szTitle,
		dwStyle,
		iX, iY, iWidth, iHeight,
		GetDesktopWindow(),
		NULL,
		GetModuleHandle(0),
		NULL);

	SetWindowLongPtr(m_hWindow, GWLP_USERDATA, (LONG_PTR)this);

	ShowWindow(m_hWindow, SW_SHOW);
	SetForegroundWindow(m_hWindow);
	SetFocus(m_hWindow);
	UpdateWindow(m_hWindow);
}

void CWindowNT::InitContext()
{
	if (m_bFullscreen)
	{
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = m_iWidth;
		dmScreenSettings.dmPelsHeight = m_iHeight;
		dmScreenSettings.dmBitsPerPel = m_iPixelBits;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			if (MessageBox(NULL, "The requested fullscreen mode is not supported by your video card. Use windowed mode instead?", "Nehe GL", MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
			{
				m_bFullscreen = false;
			}
		}
	}

	static PIXELFORMATDESCRIPTOR pfd = 
	{
		sizeof (PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		m_iPixelBits,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		16,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	m_hDeviceContext = GetDC(m_hWindow);

	GLuint uiPixelFormat = ChoosePixelFormat(m_hDeviceContext, &pfd);
	SetPixelFormat(m_hDeviceContext, uiPixelFormat, &pfd);

	m_hRenderingContext = wglCreateContext(m_hDeviceContext);
	wglMakeCurrent(m_hDeviceContext, m_hRenderingContext);

	Resize(m_iWidth, m_iHeight);
}

void CWindowNT::InitOpenGL()
{
	glShadeModel(GL_SMOOTH);
	glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

bool CWindowNT::Tick()
{
	MSG message;

	/*memcpy(m_opKeyboard->m_bPrevKeys, m_opKeyboard->m_bCurrKeys, sizeof(m_opKeyboard->m_bPrevKeys));
	memcpy(m_opMouse->m_bPrevButtons, m_opMouse->m_bCurrButtons, sizeof(m_opMouse->m_bPrevButtons));*/

	while (PeekMessage(&message, 0, 0, 0, PM_NOREMOVE))
	{
		if (!GetMessage(&message, 0, 0, 0))
		{
			return false;
		}

		TranslateMessage (&message);
		DispatchMessage (&message);
	}

	return true;
}

void CWindowNT::Resize(int a_iWidth, int a_iHeight)
{
	if (a_iHeight == 0)
	{
		a_iHeight = 1;
	}

	glViewport(0, 0, a_iWidth, a_iHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)a_iWidth/(GLfloat)a_iHeight, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void CWindowNT::Clear()
{
	SwapBuffers(m_hDeviceContext);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

LRESULT CALLBACK CWindowNT::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	CWindowNT *WindowPtr = (CWindowNT*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	if (WindowPtr == NULL)
	{
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	else
	{
		return WindowPtr->MessageProc(hWnd, msg, wParam, lParam);
	}
}

LRESULT CALLBACK CWindowNT::MessageProc(HWND hWindow, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	switch(uMessage)
	{
	case WM_ACTIVATE:
		{
			if (!HIWORD(wParam))
			{
				m_bHasFocus = true;
			}
			else
			{
				m_bHasFocus = false;
			}

			return 0;
		}
	case WM_SYSCOMMAND:
		{
			switch(wParam)
			{
			case SC_SCREENSAVE:
			case SC_MONITORPOWER:
				return 0;
			case SC_CLOSE:
				PostMessage(m_hWindow, WM_DESTROY, 0, 0);
			}

			break;
		}
	case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			break;
		}
	/*case WM_KEYDOWN:
		{
			keys[wParam] = true;
			return 0;
		}
	case WM_KEYUP:
		{
			keys[wParam] = false;
			return 0;
		}*/
	case WM_SIZE:
		{
			Resize(LOWORD(lParam), HIWORD(lParam));
			return 0;
		}
	}

	return DefWindowProc(hWindow, uMessage, wParam, lParam);
}

/*
if (keys[VK_F1])
			{
				keys[VK_F1] = false;
				KillGLWindow();
				fullscreen = !fullscreen;

				int width = 1280;
				int height = 720;

				if (fullscreen)
				{
					width = 1920;
					height = 1080;
				}

				if (!CreateGLWindow("NeHe's OpenGL Framework", width, height, 16, fullscreen))
				{
					return 0;
				}
			}
		}
*/

/*
LRESULT CALLBACK Liquid::Window::MessageProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_SYSCOMMAND:
			{
				if (wParam == SC_CLOSE)
				{
					PostMessage(hWnd, WM_DESTROY, 0, 0);
				}
				break;
			}
		case WM_ACTIVATE:
			{
				if (!HIWORD(wParam))
				{
					m_bHasFocus = true;
				}
				else
				{
					m_bHasFocus = false;
				}
				break;
			}
		case WM_DESTROY:
			{
				PostQuitMessage(0);
				break;
			}
		case WM_KEYDOWN:
			{
				if(m_opKeyboard)
					m_opKeyboard->m_bCurrKeys[wParam] = true;
				break;
			}
		case WM_KEYUP:
			{
				if(m_opKeyboard)
					m_opKeyboard->m_bCurrKeys[wParam] = false;
				break;
			}
		case WM_LBUTTONUP:
			{
				if (m_opMouse)
					m_opMouse->m_bCurrButtons[Mouse::BUTTON_LEFT] = false;
				break;
			}
		case WM_RBUTTONUP:
			{
				if (m_opMouse)
					m_opMouse->m_bCurrButtons[Mouse::BUTTON_RIGHT] = false;
				break;
			}
		case WM_MBUTTONUP:
			{
				if (m_opMouse)
					m_opMouse->m_bCurrButtons[Mouse::BUTTON_MIDDLE] = false;
				break;
			}
		case WM_LBUTTONDOWN:
			{
				if (m_opMouse)
					m_opMouse->m_bCurrButtons[Mouse::BUTTON_LEFT] = true;
				break;
			}
		case WM_RBUTTONDOWN:
			{
				if (m_opMouse)
					m_opMouse->m_bCurrButtons[Mouse::BUTTON_RIGHT] = true;
				break;
			}
		case WM_MBUTTONDOWN:
			{
				if (m_opMouse)
					m_opMouse->m_bCurrButtons[Mouse::BUTTON_MIDDLE] = true;
				break;
			}
		case WM_MOUSEWHEEL:
			{
				if (m_opMouse)
					m_opMouse->m_sWheel += (short)HIWORD(wParam);
				break;
			}
		case WM_SIZE:
			{
				return 0;
			}
	};

	return DefWindowProc(hWnd, msg, wParam, lParam);
}
*/