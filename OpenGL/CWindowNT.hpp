#ifndef _OMNI_CWINDOWNT_HPP_
#define _OMNI_CWINDOWNT_HPP_

#include "IWindow.hpp"

#include <Windows.h>

class CWindowNT
{
public:
	CWindowNT(char* a_szTitle, int a_iWidth, int a_iHeight, int a_iBits, bool a_bFullscreen);
	~CWindowNT();

	bool Tick();
	inline bool HasFocus() { return m_bHasFocus; }

	inline
	bool IsFullscreen()
	{
		return m_bFullscreen;
	}

	void Resize(int a_iWidth, int a_iHeight);

	void Clear();

protected:
private:
	CWindowNT() { };
	CWindowNT(const CWindowNT& ac_rkCopy) { }
	
	LRESULT CALLBACK MessageProc(HWND hWindow, UINT uMessage, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK WindowProc(HWND hWindow, UINT uMessage, WPARAM wParam, LPARAM lParam);

	void InitWindow();
	void InitContext();
	void InitOpenGL();

	bool m_bHasFocus;
	bool m_bFullscreen;

	char* m_szClassName;
	char* m_szTitle;

	int m_iWidth;
	int m_iHeight;

	int m_iPixelBits;

	HINSTANCE m_hInstance;
	HWND m_hWindow;
	ATOM m_sWindowClass;

	HGLRC m_hRenderingContext;
	HDC m_hDeviceContext;
};

#endif